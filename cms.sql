-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 27 2017 г., 20:00
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(140) NOT NULL,
  `text` mediumtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `text`, `date`) VALUES
(1, 'James Hetfields Reaction To HULK HOGANs Claim That He Auditioned For METALLICA Is Priceless', 'PHAgc3R5bGU9InRleHQtYWxpZ246Y2VudGVyIj48aW1nIGFsdD0iIiBzcmM9Imh0dHBzOi8vaTEud3AuY29tL3d3dy5tZXRhbGluamVjdGlvbi5uZXQvd3AtY29udGVudC91cGxvYWRzLzIwMTcvMDYvbWV0YWxsaWNhLWh1bGstaG9nYW4uanBnP2ZpdD03MDAlMkMzNjciIC8+PC9wPg0KDQo8cD5PbmUgb2YgbXkgZmF2b3JpdGUgc3RvcmllcyB0byBjb21lIGJhY2sgdG8gaXMgcmV0aXJlZCBwcm9mZXNzaW9uYWwgd3Jlc3RsZXIgSHVsayBIb2dhbiYjMzk7cyBvdXRyYWdlb3VzIGNsYWltcyB0aGF0IGhlIG9uY2UgYXVkaXRpb25lZCB0byBiZSBpbiBNZXRhbGxpY2EgdG8gcmVwbGFjZSBDbGlmZiBCdXJ0b24uIEl0IGFsbCBzdGFydGVkIHdpdGggYSBxdW90ZSBoZSBnYXZlIHRvIHRoZSBVSyB0YWJsb2lkLCZuYnNwOzxlbT5UaGUgU3VuPC9lbT4mbmJzcDthYm91dCBzZXZlbiB5ZWFycyBhZ286PC9wPg0KDQo8YmxvY2txdW90ZT4NCjxwPiZxdW90O0kgdXNlZCB0byBiZSBhIHNlc3Npb24gbXVzaWNpYW4gYmVmb3JlIEkgd2FzIGEgd3Jlc3RsZXIuIEkgcGxheWVkIGJhc3MgZ3VpdGFyLiBJIHdhcyBiaWcgcGFscyB3aXRoIExhcnMgVWxyaWNoIGFuZCBoZSBhc2tlZCBtZSBpZiBJIHdhbnRlZCB0byBwbGF5IGJhc3Mgd2l0aCBNZXRhbGxpY2EgaW4gdGhlaXIgZWFybHkgZGF5cyBidXQgaXQgZGlkbiYjMzk7dCB3b3JrIG91dC4mcXVvdDs8L3A+DQo8L2Jsb2NrcXVvdGU+DQoNCjxwPlRoZSBzdG9yeSBnb3QgcmVhbGx5IGJpZyBhbmQgZXZlbiBIb3dhcmQgU3Rlcm4gY2F1Z2h0IHdpbmQgb2YgaXQsIHNvIG9mIGNvdXJzZSB0aGUgbmV4dCB0aW1lIE1ldGFsbGljYSBkcnVtbWVyIExhcnMgVWxyaWNoIHdhcyBpbiBzdHVkaW8sIFN0ZXJuIGFza2VkIGhpbSBhYm91dCBpdC4gTGFycyYjMzk7Jm5ic3A7PGEgaHJlZj0iaHR0cDovL3d3dy5tZXRhbGluamVjdGlvbi5uZXQvbGF0ZXN0LW5ld3MvbWV0YWxsaWNhcy1sYXJzLXVscmljaC1jYWxscy1odWxrLWhvZ2FuLWEtbGlhciI+cmVzcG9uc2Ugd2FzPC9hPiZuYnNwO2Jhc2ljYWxseSAmcXVvdDtIdWxrIHdobz8mcXVvdDsgTGFycyBzYWlkIGhlJiMzOTtkIG5ldmVyIG1ldCB0aGUgZ3V5LjwvcD4NCg0KPHA+U28gbmF0dXJhbGx5LCB3aGVuIEh1bGsgJnF1b3Q7PGEgaHJlZj0iaHR0cDovL3d3dy5taXJyb3IuY28udWsvM2FtL2NlbGVicml0eS1uZXdzL2h1bGstaG9nYW4tc2F5cy1kb2VzbnQtMTAtNzUyMjgxMSIgb25jbGljaz0iX19nYVRyYWNrZXIoJ3NlbmQnLCAnZXZlbnQnLCAnb3V0Ym91bmQtYXJ0aWNsZScsICdodHRwOi8vd3d3Lm1pcnJvci5jby51ay8zYW0vY2VsZWJyaXR5LW5ld3MvaHVsay1ob2dhbi1zYXlzLWRvZXNudC0xMC03NTIyODExJywgJzEwIEluY2ggUGVuaXMnKTsiIHRhcmdldD0iX2JsYW5rIj4xMCBJbmNoIFBlbmlzPC9hPiZxdW90OyBIb2dhbiBnZXRzIGNhbGxlZCBvdXQgb24gaGlzIGJ1bGxzaGl0LCBoZSBjaGFuZ2VkIGhpcyBzdG9yeSBhIGJpdCwgYXMgbm90ZWQgZHVyaW5nIHRoaXMgaW50ZXJ2aWV3IHdpdGggTm9pc2V5IG9uIEh1bGsmIzM5O3MgbXVzaWMgdmVudHVyZXM6PC9wPg0KDQo8YmxvY2txdW90ZT4NCjxwPkkgaGVhcmQgdGhhdCBNZXRhbGxpY2EgbmVlZGVkIGEgYmFzcyBwbGF5ZXIsIGFuZCBicm90aGVyLCBJIHdhcyB3cml0aW5nIGxldHRlcnMsIG1hZGUgYSB0YXBlIG9mIG15c2VsZiBwbGF5aW5nIGFuZCBzZW50IGl0IHRvIHRoZWlyIG1hbmFnZW1lbnQgY29tcGFueS4gS2VwdCBtYWtpbmcgY2FsbHMgdHJ5aW5nIHRvIGdldCB0aHJvdWdoLiBJIHRyaWVkIGZvciB0d28gd2Vla3MgYW5kIG5ldmVyIGhlYXJkIGEgd29yZCBiYWNrIGZyb20gdGhlbSBlaXRoZXIuIFsuLl0gSSB3YXMgaG9waW5nIGZvciBhIGNhbGwgZnJvbSB0aGVtIGJ1dCBuZXZlciBnb3Qgb25lLiBBbGwgdGhlIGhhdGVycyB3ZXJlIGxpa2UgJmxkcXVvO1lvdSBuZXZlciBhdWRpdGlvbmVkIGZvciBNZXRhbGxpY2EhJnJkcXVvOyBPZiBjb3Vyc2UgSSBkaWRuJnJzcXVvO3QmbWRhc2g7YnV0IEkgdHJpZWQhPC9wPg0KPC9ibG9ja3F1b3RlPg0KDQo8cD5TbyBIdWxrIHdlbnQgZnJvbSBiZWluZyAmcXVvdDtiaWcgcGFscyZxdW90OyB3aXRoIExhcnMgVWxyaWNoIHRvIGdvaW5nJm5ic3A7PGVtPm9mIGNvdXJzZSBJIG5ldmVyIGFjdHVhbGx5IGF1ZGl0aW9uZWQ8L2VtPi4gR29vZCBzdHVmZi4gQnV0IGV2ZW4gYmV0dGVyIGlzIHRoaXMgaW50ZXJ2aWV3IHRoYXQgc3VyZmFjZWQgZnJvbSBEZWNlbWJlciwgd2hlcmUgSmFtZXMgSGV0ZmllbGQgaXMgZ2l2ZW4gYSBidW5jaCBvZiBwaG90b3MgYW5kIGFza2VkIHRvIHJlYWN0LiBUaGUgaGlnaGxpZ2h0IGlzIHdoZW4gaGUgaXMgZ2l2ZW4gYSBwaG90b3Nob3BwZWQgcGhvdG8gKGNyZWF0ZWQgYnkgeW91cnMgdHJ1bHkpIG9mIHRoZSBiYW5kIGFuZCBNZXRhbGxpY2EgdG9nZXRoZXIgYW5kIEhldGZpZWxkIGlzIGFsbW9zdCBhdCBhIGxvc3MgZm9yIHdvcmRzIGFzIHRvIGhvdyB0byByZWFjdC48L3A+DQoNCjxwPiZxdW90O0kgZG9uJiMzOTt0IGtub3cgW0hvZ2FuXSYjMzk7cyB2ZXJzaW9uIG9mIGhpc3RvcnkuIEkgZG9uJiMzOTt0IHJlbWVtYmVyIGhpbS4mcXVvdDsgSGV0ZmllbGQgc2F5cyB3aXRoIGEgbGF1Z2guICZxdW90O1doYXQgaXMgaXQ/IEhlIHdhcyBpbiB0aGUgYmFuZCBmb3IgYSBtaW51dGU/JnF1b3Q7PC9wPg0KDQo8cD5XaGVuIHRoZSBpbnRlcnZpZXdlciBleHBsYWlucyB0aGF0IEhvZ2FuIHNhaWQgaGUgYXVkaXRpb25lZCBmb3IgdGhlIGJhbmQsIEhldGZpZWxkIHJlc3BvbmRzICZxdW90O0h1aD8gRGVmaW5pdGVseSBub3QuIEJ1dCBhbnl3YXksIGl0JiMzOTtzIGEgZ29vZCBmaXQsIHllYWg/IFtMYXVnaHNdIEhlIG1ha2VzIHVzIGxvb2sgdmVyeSBzbWFsbC4mcXVvdDs8L3A+DQo=', '2017-06-25 21:00:00'),
(4, 'OZZY OSBOURNE Lists His Top 10 Favorite Metal Albums', 'PHAgc3R5bGU9InRleHQtYWxpZ246Y2VudGVyIj48aW1nIGFsdD0iIiBzcmM9Imh0dHBzOi8vaTAud3AuY29tL3d3dy5tZXRhbGluamVjdGlvbi5uZXQvd3AtY29udGVudC91cGxvYWRzLzIwMTUvMDMvb3p6eS1vc2JvdXJuZS5qcGc/Zml0PTcwMCUyQzQ2NiIgLz48L3A+DQoNCjxwPiZuYnNwOzwvcD4NCg0KPHA+T3p6eSBPc2JvdXJuZSBpcyBhIGxpdmluZyBsZWdlbmQuIFRoZSBtYW4gaXMgcGFydGlhbGx5IHJlc3BvbnNpYmxlIGZvciB0aGUgYmlydGggb2YgaGVhdnkgbWV0YWwuIFNvIHdoYXQgYXJlIGhpcyBmYXZvcml0ZSBhbGJ1bXMgZnJvbSB0aGUgZ2VucmUgaGUgaGVscGVkIGNyZWF0ZT88L3A+DQoNCjxwPjxlbT5Sb2xsaW5nIFN0b25lPC9lbT4mbmJzcDtjb250aW51ZWQgcHVibGlzaGluZyBpbmRpdmlkdWFsIGxpc3RzIG9mIG11c2ljaWFucyB0aGV5IGFza2VkIHRvIGNvbnRyaWJ1dGUgdG8gdGhlaXImbmJzcDs8YSBocmVmPSJodHRwOi8vd3d3Lm1ldGFsaW5qZWN0aW9uLm5ldC9sYXRlc3QtbmV3cy9ibGFjay1zYWJiYXRoLW1ldGFsbGljYS1qdWRhcy1wcmllc3QtdG9wLXRoZS0xMDAtZ3JlYXRlc3QtbWV0YWwtYWxidW1zLW9mLWFsbC10aW1lLWJ5LXJvbGxpbmctc3RvbmVzIj4xMDAgR3JlYXRlc3QgSGVhdnkgTWV0YWwgQWxidW1zPC9hPiZuYnNwO2xpc3QsIGluY2x1ZGluZyBsaXN0cyBmcm9tJm5ic3A7PGEgaHJlZj0iaHR0cDovL3d3dy5tZXRhbGluamVjdGlvbi5uZXQvbGlzdHMvbWV0YWxsaWNhcy1sYXJzLXVscmljaC1saXN0cy1oaXMtMTUtZmF2b3JpdGUtbWV0YWwtYW5kLWhhcmQtcm9jay1hbGJ1bXMiPkxhcnMgVWxyaWNoPC9hPiZuYnNwO2FuZCZuYnNwOzxhIGhyZWY9Imh0dHA6Ly93d3cubWV0YWxpbmplY3Rpb24ubmV0L2xpc3RzL2p1ZGFzLXByaWVzdHMtcm9iLWhhbGZvcmQtbGlzdHMtaGlzLTEwLWZhdm9yaXRlLW1ldGFsLWFsYnVtcyI+Um9iIEhhbGZvcmQ8L2E+LiBIZXJlJiMzOTtzIE96enkmIzM5O3MgbGlzdDo8L3A+DQoNCjxibG9ja3F1b3RlPg0KPHA+PHN0cm9uZz5BQy9EQzwvc3Ryb25nPiZuYnNwOyZuZGFzaDsmbmJzcDs8ZW0+SGlnaHdheSBUbyBIZWxsPC9lbT48YnIgLz4NCjxzdHJvbmc+QWxpY2UgSW4gQ2hhaW5zPC9zdHJvbmc+Jm5ic3A7Jm5kYXNoOyZuYnNwOzxlbT5GYWNlbGlmdDwvZW0+PGJyIC8+DQo8c3Ryb25nPkd1bnMgTiZyc3F1bzsgUm9zZXM8L3N0cm9uZz4mbmJzcDsmbmRhc2g7Jm5ic3A7PGVtPkFwcGV0aXRlIEZvciBEZXN0cnVjdGlvbjwvZW0+PGJyIC8+DQo8c3Ryb25nPkp1ZGFzIFByaWVzdDwvc3Ryb25nPiZuYnNwOyZuZGFzaDsmbmJzcDs8ZW0+QnJpdGlzaCBTdGVlbDwvZW0+PGJyIC8+DQo8c3Ryb25nPkxlZCBaZXBwZWxpbjwvc3Ryb25nPiZuYnNwOyZuZGFzaDsmbmJzcDs8ZW0+TGVkIFplcHBlbGluIElWPC9lbT48YnIgLz4NCjxzdHJvbmc+TWVnYWRldGg8L3N0cm9uZz4mbmJzcDsmbmRhc2g7Jm5ic3A7PGVtPlJ1c3QgSW4gUGVhY2U8L2VtPjxiciAvPg0KPHN0cm9uZz5NZXRhbGxpY2E8L3N0cm9uZz4mbmJzcDsmbmRhc2g7Jm5ic3A7PGVtPk1hc3RlciBPZiBQdXBwZXRzPC9lbT48YnIgLz4NCjxzdHJvbmc+TW90Jm91bWw7cmhlYWQ8L3N0cm9uZz4mbmJzcDsmbmRhc2g7Jm5ic3A7PGVtPkFjZSBPZiBTcGFkZXM8L2VtPjxiciAvPg0KPHN0cm9uZz5QYW50ZXJhPC9zdHJvbmc+Jm5ic3A7Jm5kYXNoOyZuYnNwOzxlbT5Db3dib3lzIEZyb20gSGVsbDwvZW0+PGJyIC8+DQo8c3Ryb25nPlJvYiBab21iaWU8L3N0cm9uZz4mbmJzcDsmbmRhc2g7Jm5ic3A7PGVtPkhlbGxiaWxseSBEZWx1eGU8L2VtPjwvcD4NCjwvYmxvY2txdW90ZT4NCg0KPHA+Tm90aGluZyB0b28gc3VycHJpc2luZy4gT3p6eSBvZmZlcmVkIGEgZmV3IGRldGFpbHMgdG8mbmJzcDs8ZW0+Um9sbGluZyBTdG9uZTwvZW0+Jm5ic3A7aW5jbHVkaW5nIGNhbGxpbmcgSnVkYXMgUHJpZXN0ICZxdW90O29uZSBvZiB0aGUgYmVzdCBtZXRhbCBiYW5kcyBvZiBhbGwgdGltZSZxdW90OyBhbmQgY2FsbGluZyBQYW50ZXJhICZxdW90O3NvbWUgb2YgdGhlIGJpZ2dlc3QgcGFydHkgYW5pbWFscyBJIGV2ZXIgdG91cmVkIHdpdGgsJnF1b3Q7IHdoaWNoIGNvbWluZyBmcm9tIE96enksIGlzIGluY3JlZGlibHkgZmxhdHRlcmluZy48L3A+DQo=', '2017-06-25 21:00:00'),
(5, 'Police Says Rock Am Ring Was Not Evacuated Due To A Spelling Error', 'PHAgc3R5bGU9InRleHQtYWxpZ246Y2VudGVyIj48aW1nIGFsdD0iIiBzcmM9Imh0dHBzOi8vaTEud3AuY29tL3d3dy5tZXRhbGluamVjdGlvbi5uZXQvd3AtY29udGVudC91cGxvYWRzLzIwMTQvMDYvUm9ja0FtUmluZy5qcGc/Zml0PTQ4NSUyQzI3MCIgLz48L3A+DQoNCjxwPiZuYnNwOzwvcD4NCg0KPHA+T24gSnVuZSAyLCB0aGUgUm9jayBBbSBSaW5nIGZlc3RpdmFsIGluIEdlcm1hbnkgd2FzJm5ic3A7PGEgaHJlZj0iaHR0cDovL3d3dy5tZXRhbGluamVjdGlvbi5uZXQvbGF0ZXN0LW5ld3MvYnVtbWVyLWFsZXJ0L2dlcm1hbnlzLXJvY2stYW0tcmluZy1mZXN0aXZhbC1ldmFjdWF0ZWQtZHVlLXRvLXRlcnJvci13YXJuaW5nIj5ldmFjdWF0ZWQgZHVlIHRvIGEgdGVycm9yIHRocmVhdDwvYT4uIEl0IHdhcyBsYXRlciBzdGF0ZWQgdGhhdCB0aGUgdGVycm9yIHRocmVhdCZuYnNwOzxhIGhyZWY9Imh0dHA6Ly93d3cubWV0YWxpbmplY3Rpb24ubmV0L21ldGFsLWNyaW1lcy9yb2NrLWFtLXJpbmctdGVycm9yaXN0LXdhcm5pbmctdHVybmVkLW91dC10by1iZS1zcGVsbGluZy1taXN0YWtlIj5zdGVtbWVkIGZyb20gYSBzcGVsbGluZyBlcnJvcjwvYT4mbmJzcDtvbiB0aGUgbGlzdCBvZiB3aG8gc2hvdWxkIGJlIGJhY2sgc3RhZ2UsIHRob3VnaCBub3cgaXQmIzM5O3MgY29taW5nIHRvIGxpZ2h0IHRoYXQgdGhpcyB3YXNuJiMzOTt0IGp1c3QgdGhlIGNhc2UsIGFuZCB0aGVyZSB3YXMgYSB2ZXJ5IHJlYWwgcG9zc2liaWxpdHkgb2YgYSB0ZXJyb3IgYXR0YWNrLjwvcD4NCg0KPHA+QWNjb3JkaW5nIHRvJm5ic3A7PGEgaHJlZj0iaHR0cHM6Ly93d3cucG9sbHN0YXIuY29tL2FydGljbGUvY29uZnVzaW9uLW92ZXItcm9jay1hbS1yaW5nLWV2YWMtMTMyMDY0IiBvbmNsaWNrPSJfX2dhVHJhY2tlcignc2VuZCcsICdldmVudCcsICdvdXRib3VuZC1hcnRpY2xlJywgJ2h0dHBzOi8vd3d3LnBvbGxzdGFyLmNvbS9hcnRpY2xlL2NvbmZ1c2lvbi1vdmVyLXJvY2stYW0tcmluZy1ldmFjLTEzMjA2NCcsICdQb2xsc3RhcicpOyI+UG9sbHN0YXI8L2E+LCBwb2xpY2UgdW5jb3ZlcmVkIHRocmVlIHN1c3BlY3RzIGR1cmluZyBhIHJvdXRpbmcgc2VjdXJpdHkgY2hlY2sgb2YgdGhlIGZlc3RpdmFsLCB0d28gb2Ygd2hvbSBoYXMgUm9jayBBbSBSaW5nIHdyaXN0YmFuZHMuIFRoZSB0d28gc3VzcGVjdHMgaGFkIG5hbWVzIHRoYXQgd2VyZSBzaW1pbGFyIHRvIGVtcGxveWVlcyBvZiBMaXZlIE5hdGlvbiwgYnV0IHdlcmUgbm90IG9uIHRoZSBsaXN0LjwvcD4NCg0KPHA+VGhlIHN1c3BlY3RzIHdlcmUgYXJyZXN0ZWQsIGFuZCBwb2xpY2UgYXJlIG5vdyBzYXlpbmcgJnF1b3Q7SXQgd2FzbiZyc3F1bzt0IGEgc3BlbGxpbmcgbWlzdGFrZSB0aGF0IGxlZCB0byB0aGUgZXZhY3VhdGlvbiBvZiB0aGUgZmVzdGl2YWwgc2l0ZS4gQWN0dWFsIGNvbm5lY3Rpb25zIG9mIG9uZSBmZXN0aXZhbCBlbXBsb3llZSB0byB0aGUgSXNsYW1pYyByZWFsbSB3ZXJlIGluZGVlZCB0aGUgcmVhc29uLiZxdW90OzwvcD4NCg0KPHA+SGVpZ2h0ZW5lZCBzZWN1cml0eSBpbiB0aGUgYXJlYSBhbHNvIG1ha2VzIHNlbnNlLCBjb25zaWRlcmluZyB0aGUgTWFuY2hlc3RlciBhdHRhY2tzIGhhZCBqdXN0IGhhcHBlbmVkIG9uIE1heSAyMi4gRm9ydHVuYXRlbHkgbm90aGluZyBlbmRlZCB1cCBoYXBwZW5pbmcgYXQgdGhlIGZlc3RpdmFsLCBidXQgc3RpbGwgJm5kYXNoOyBnb29kIG9uIHNlY3VyaXR5IGZvciBiZWluZyBzbyBvbiB0b3Agb2YgdGhpbmdzLjwvcD4NCg==', '2017-06-25 21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `song`
--

CREATE TABLE `song` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `artist` varchar(30) NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `song`
--

INSERT INTO `song` (`id`, `title`, `artist`, `url`) VALUES
(2, 'Silvera', 'Gojira', 'template/resource/files/audio/02 - Silvera.mp3'),
(3, 'They Say', 'Scars on Broadway', 'template/resource/files/audio/15 - They Say.mp3'),
(4, 'Kill Each Other-Live Forever', 'Scars on Broadway', 'template/resource/files/audio/07 - Kill Each Other-Live Forever.mp3'),
(5, 'Swerve City', 'Deftones', 'template/resource/files/audio/01. Swerve City.mp3'),
(26, 'Track 13', 'Dry Kill Logic', 'template/resource/files/audio/09. Track 13.mp3'),
(30, 'Psycho', 'Muse', 'template/resource/files/audio/03. Psycho.mp3'),
(31, 'Toxicity', 'System Of A Down', 'template/resource/files/audio/12 - Toxicity.mp3');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1, 'Arthur', 'arti.melnik666@gmail.com', 'e61eb81163c8840a02f8be80d9e69fbb');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `song`
--
ALTER TABLE `song`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
