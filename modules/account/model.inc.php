<?php
class Account_Model
{
    public function Insert($row)
    {
        Core::$Db->Insert('user', $row);
    }
    public function Select($email, $id)
    {
        $arr = ['email ' => $email,
        'id !' => $id];
        return Core::$Db->Select('user', 'count(email) as duplicate', $arr);
    }
    public function SelectByID($id)
    {
        $arr = ['id ' => $id];
        return Core::$Db->Select('user', ['name', 'email'], $arr);
    }
    public  function UpdateById($id, $row)
    {
        Core::$Db->UpdateById('user', $row, 'id', $id);
    }
    public function DeleteById($id)
    {
        Core::$Db->DeleteById('user', 'id', $id);
    }
}