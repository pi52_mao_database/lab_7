<?php
class Account_Controller
{
    public function IndexAction()
    {
        if(isset($_SESSION['user_id'])) //доступ лише у разі, коли користувач увійшов, в іншому випадку перенаправлення на форму входу
        {
        $status = null;
        $view = new Account_View();
        $model = new Account_Model();
        $content = $view->Index();
        $content->SetParam('item', $model->SelectByID($_SESSION['user_id'])); //вибірка даних про поточного користувача
        if ($_SERVER['REQUEST_METHOD'] == "POST") // дія при запиті зі сторони користувача
        {
            $status = null;
            $row = $_POST;
            if($row['password'] == $row['confirm_password']) { //перевірка, чи пароль і його підтвердження співпадають, для запобігання введення помилкового паролю
                unset($row['confirm_password']);
                if (strlen($row['password']) >= 6 and strlen($row['password']) <= 25) { //перевірка довжини пароля
                    $duplicate = $model->Select($row['email'],  $_SESSION['user_id']); //робимо вибірку по email, щоб дізнатись, чи немає такого ж в базі даних
                    if ($duplicate[0]['duplicate'] == '0') { //в разі дублювання email, користувач сповіщається, що такий email вже зайнятий
                        $row['password'] = md5($row['password']); //шифруємо новий пароль за допомогою md5
                        $model->UpdateById( $_SESSION['user_id'] ,$row); //вносимо в базу
                        $content->SetParam('item', $model->SelectByID( $_SESSION['user_id'])); //оновлюємо дані на сторінці
                        $status = "Готово!";
                    } else {
                        $status = "Такий користувач вже існує!";
                    }
                } else{
                    $status = "Пароль повинен складатися як мінімум з 6 і максимум 25 символів";
                }
            } else {
                $status = "Паролі не співпадають!";
            }
        }
        return array(
            "PageTitle" => "Мій акаунт",
            "PageHeaderTitle" => "Редагувати данні",
            "Content" => $content->GetHTML(),
            "Status" => $status
        );
        }
        else{
            header('Location: /login/');
            return null;
        }
    }
    public function DeleteAction()
    {
        if(isset($_SESSION['user_id']))
        {
            $model = new Account_Model();
            $model->DeleteById($_SESSION['user_id']); //видалення акаунта користувача
            session_destroy(); //знищення сесії
            header('Location: /');
        }else{ header('Location: /login/');}
    }
    public function OutAction()
    {
        session_destroy();
        header('Location: /');
    }
}