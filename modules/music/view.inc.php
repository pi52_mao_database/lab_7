<?php
class Music_View
{
    public function Add()
    {
        $tpl = new Template('template/music/add.tpl');
        return $tpl->GetHTML();
    }
    public function Index()
    {
        $tpl = new Template('template/music/index.tpl');
        return $tpl;
    }
    public function Edit()
    {
        $tpl = new Template('template/music/edit.tpl');
        return $tpl;
    }

}