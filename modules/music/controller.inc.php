<?php
class Music_Controller
{
    public function EditAction($params)
    {
        if (isset($_SESSION['user_id'])) {
            $status = null;
            $view = new Music_View();
            $model = new Music_Model();
            $content = $view->Edit();
            $content->SetParam('item', $model->Select($params[0])); //вибірка даних з БД, для внесення в поля редагування
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $row = $_POST;
                $model->UpdateById($params[0], $row); //оновлення даних
                $status = "Готово!";
                $content->SetParam('item', $model->Select($params[0])); //оновлення даних на сторінці
            }
            return array(
                "PageTitle" => "Редагувати пісню",
                "PageHeaderTitle" => "Редагувати інформацію про пісню",
                "Content" => $content->GetHTML(),
                "Status" => $status
            );
        } else {
            header('Location: /login/');
            return null;
        }
    }

    public function AddAction()
    {
        if (isset($_SESSION['user_id'])) {
            $status = null;
            $view = new Music_View();
            $model = new Music_Model();
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $row = $_POST;

                $uploadDir = 'template/resource/files/audio/'; //дирикторія на сервері для збереження аудіо
                $uploadFile = $uploadDir . basename($_FILES['audio']['name']); //повний шдях, з назвою пісні, по якому буде збережено аудіо

                if (move_uploaded_file($_FILES['audio']['tmp_name'], $uploadFile)) { //якщо вдалося завантажити і перемістити аудіо
                    $status = "Пісню додано!";
                    $row['url'] = $uploadFile;//внесення силки на пісню для збереження в БД
                    $model->Insert($row);
                } else {
                    $status = "Завантаження не вдалось.";
                }
            }
            return array(
                "PageTitle" => "Додати пісню",
                "PageHeaderTitle" => "Додати пісню",
                "Content" => $view->Add(),
                "Status" => $status
            );
        } else {
            header('Location: /login/');
            return null;
        }
    }

    public function IndexAction()
    {
        $view = new Music_View();
        $model = new Music_Model();
        $content = $view->Index();
        $content->SetParam('Music', $model->Select());
        if (isset($_SESSION['user_id'])) {
            $content->SetParam('Account', 'logged');
        }
        return array(
            "PageTitle" => "Музика",
            "PageHeaderTitle" => "Аудіозаписи",
            "Content" => $content->GetHTML()
        );
    }

    public function DeleteAction($params)
    {
        if (isset($_SESSION['user_id'])) {
            $model = new Music_Model();
            $song = $model->Select($params[0]);
            unlink($song[0]['url']); //видаляємо сам файл
            $model->DeleteById($params[0]);
            die(); //видалення за допомогою асинхронних запитів
        } else {
            header('Location: /login/');
            return null;
        }
    }
}