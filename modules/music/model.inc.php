<?php
class Music_Model
{
    public function DeleteById($id)
    {
        Core::$Db->DeleteById('song', 'id', $id);
    }

    public function Insert($row)
    {
        Core::$Db->Insert('song', $row);
    }

    public function Select($id = null)
    {
        $arr = null;
        if(!is_null($id)) {
            $arr = ['id ' => $id];
        }
        return Core::$Db->Select('song', ['id', 'title', 'artist', 'url'], $arr);
    }
    public  function UpdateById($id, $row)
    {
        Core::$Db->UpdateById('song', $row, 'id', $id);
    }
}