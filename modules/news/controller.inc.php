<?php
class News_Controller
{
    public function EditAction($params)
    {
        if (isset($_SESSION['user_id'])) {
            $status = null;
            $view = new News_View();
            $model = new News_Model();
            $content = $view->Edit();
            $content->SetParam('item', $model->Select($params[0]));
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $row = $_POST;
                $row['text'] = base64_encode($row['text']);//кодування тексту новини (яка містить html, для форматування) в base64 для уникнення проблем при внесені в БД
                $model->UpdateById($params[0], $row);
                $content->SetParam('item', $model->Select($params[0]));
                $status = "Готово!";
            }
            return array(
                "PageTitle" => "Редагувати новину",
                "PageHeaderTitle" => "Редагувати новину",
                "Content" => $content->GetHTML(),
                "Status" => $status
            );
        } else {
            header('Location: /login/');
            return null;
        }
    }

    public function AddAction()
    {
        if (isset($_SESSION['user_id'])) {
            $status = null;
            $view = new News_View();
            $model = new News_Model();
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $row = $_POST;
                $row['text'] = base64_encode($row['text']);
                $model->Insert($row);
                $status = "Новину додано!";
            }
            return array(
                "PageTitle" => "Додати новину",
                "PageHeaderTitle" => "Додати новину",
                "Content" => $view->Add(),
                "Status" => $status
            );
        } else {
            header('Location: /login/');
            return null;
        }
    }

    public function IndexAction()
    {
        $view = new News_View();
        $model = new News_Model();
        $content = $view->Index();
        $content->SetParam('News', $model->Select());
        if (isset($_SESSION['user_id'])) {
            $content->SetParam('Account', 'logged');
        }
        return array(
            "PageTitle" => "Новини",
            "PageHeaderTitle" => "Новини",
            "Content" => $content->GetHTML()
        );
    }

    public function DeleteAction($params)
    {
        if (isset($_SESSION['user_id'])) {
            $model = new News_Model();
            $model->DeleteById($params[0]);
            die(); //видалення за допомогою асинхронних запитів
        } else {
            header('Location: /login/');
            return null;
        }
    }
}