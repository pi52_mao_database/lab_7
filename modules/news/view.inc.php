<?php
class News_View
{
    public function Add()
    {
        $tpl = new Template('template/news/add.tpl');
        return $tpl->GetHTML();
    }
    public function Index()
    {
        $tpl = new Template('template/news/index.tpl');
        return $tpl;
    }
    public function Edit()
    {
        $tpl = new Template('template/news/edit.tpl');
        return $tpl;
    }

}