<?php
class Home_Controller
{
    public function IndexAction()
    {
        $view = new Home_View();
        return array(
            "PageTitle" => "Головна",
            "PageHeaderTitle" => "Heavy Metal",
            "Content" => $view->Index()
        );
    }
}