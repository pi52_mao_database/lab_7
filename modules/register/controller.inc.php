<?php
class Register_Controller
{
    public function IndexAction()
    {
        if(isset($_SESSION['user_id']))
        {
            header('Location: /account/');
        }
        $status = null;
        $view = new Register_View();
        $model = new Register_Model();
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $status = null;
            $row = $_POST;
            if($row['password'] == $row['confirm_password']) {
                unset($row['confirm_password']);
                if (strlen($row['password']) >= 6 and strlen($row['password']) <= 25) {
                    $duplicate = $model->Select($row['email']);
                    if ($duplicate[0]['duplicate'] == '0') {
                        $row['password'] = md5($row['password']);
                        $model->Insert($row);
                        header('Location: /login/');
                    } else {
                        $status = "Такий користувач вже існує!";
                    }
                } else{
                    $status = "Пароль повинен складатися як мінімум з 6 і максимум 25 символів";
                }
            } else {
                $status = "Паролі не співпадають!";
            }
        }
        return array(
            "PageTitle" => "Реїстрація",
            "PageHeaderTitle" => "Створити акаунт",
            "Content" => $view->Index(),
            "Status" => $status
        );
    }

}