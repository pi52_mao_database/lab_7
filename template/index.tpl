<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?=$PageTitle ?></title>

    <link href="/template/resource/style/material.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/template/resource/style/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
<div class="mdl-js-layout">
    <header class="mdl-layout__header mdl-shadow--8dp">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title">The Art of Dying</span>
            <div class="mdl-layout-spacer"></div>
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="\">Головна</a>
                <a class="mdl-navigation__link" href="\news">Новини</a>
                <a class="mdl-navigation__link" href="\music">Музика</a>
                <button id="account_btn" class="mdl-button mdl-js-button">Акаунт</button>
                <div class="drop_menu">
                    <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                        for="account_btn">
                        <?php if($Account == "logged"){ echo ('<li><a class="mdl-menu__item" href="\account">Мій акаунт</a></li>');}?>
                        <?php if($Account == "out"){ echo ('<li><a class="mdl-menu__item" href="\login">Увійти</a></li>');}?>
                        <?php if($Account == "out"){ echo ('<li><a class="mdl-menu__item" href="\register">Зареєструватися</a></li>');}?>
                        <?php if($Account == "logged"){ echo ('<li><a class="mdl-menu__item" href="\account\out">Вийти</a></li>');}?>
                    </ul>
                </div>
            </nav>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">The Art of Dying</span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="\">Головна</a>
            <a class="mdl-navigation__link" href="\news">Новини</a>
            <a class="mdl-navigation__link" href="\music">Музика</a>

                        <div class="button "><a id="box1" class="mdl-navigation__link" href="#">Акаунт</a></div>
                        <div class="dropdown mdl-color--blue-grey-50" style="display: none;">
                            <div>
                                <?php if($Account == "logged"){ echo ('<a class="mdl-navigation__link" href="\account">Мій акаунт</a>');}?>
                                <?php if($Account == "out"){ echo ('<a class="mdl-navigation__link" href="\login">Увійти</a>');}?>
                                <?php if($Account == "out"){ echo ('<a class="mdl-navigation__link" href="\register">Зареєструватися</a>');}?>
                                <?php if($Account == "logged"){ echo ('<a class="mdl-navigation__link" href="\account\out">Вийти</a>');}?>
                            </div>
                        </div>

        </nav>
    </div>
<main class="mdl-layout__content mdl-shadow--8dp">
    <h1><?=$PageHeaderTitle ?></h1>
    <div><p><?=$Status?></p></div>
    <div id="ajax-result"><?=$Content ?></div>
</main>
    <footer class="mdl-mini-footer ">
        <div class="mdl-mini-footer__left-section">
            <div class="mdl-logo">The Art of Dying</div>
            <ul class="mdl-mini-footer__link-list">
            </ul>
        </div>
        <div class="mdl-mini-footer__right-section">
            <p class="mdl-logo copy">&copy Copyright by <a target="_blank" href="https://t.me/Arthur_Melnyk">Arthur Melnyk</a></p>
        </div>
    </footer>
</div>
<script src="/template/resource/js/jquery-3.2.1.min.js"></script>
<script src="/template/resource/js/material.min.js"></script>
<script src="https://cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script src="/template/resource/js/script.js"></script>
</body>
</html>