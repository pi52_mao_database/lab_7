
<form enctype="multipart/form-data" method="post">
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="artist" id="artist" value="<?=$item[0]['artist']?>" required>
        <label class="mdl-textfield__label" for="artist">Виконавець</label>
    </div>
    <div id="input_field" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
        <input class="mdl-textfield__input" type="text" name="title" id="title" value="<?=$item[0]['title']?>" required>
        <label class="mdl-textfield__label" for="title">Назва пісні</label>
    </div>
    <br>
    <audio controls src="../../<?=$item[0]['url']?>"></audio>
    <br>
    <br>
    <br>
    <input type="submit" value="Зберегти" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored btn_size"/>
</form>
