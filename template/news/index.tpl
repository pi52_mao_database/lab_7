<?php if($Account == "logged"){ echo ('<a class="mdl-button mdl-color--indigo-500 right-top" href="/news/add">Додати новину</a>');}?>
<?php foreach (array_reverse($News) as $item) { ?>
<div class="demo-card-wide mdl-card mdl-shadow--2dp">
    <div class="mdl-card__title">
        <h2 class="mdl-card__title-text"><?=$item['title']?></h2>
    </div>
    <div class="mdl-card__supporting-text">
        <?php echo(base64_decode($item['text']));?>
    </div>
    <div class="mdl-card__actions mdl-card--border mdl-textfield--align-right">
        <?=$item['date']?>
    </div>
    <div class="mdl-card__menu" <?php if($Account != "logged"){ echo (' style="display: none"');}?> >
        <button id="menu_btn_<?=$item['id']?>"
                class="mdl-button mdl-js-button mdl-button--icon">
            <i class="material-icons md-dark">more_vert</i>
        </button>

        <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
            for="menu_btn_<?=$item['id']?>">
            <li><a class="mdl-menu__item" href="/news/edit/<?=$item['id']?>">Редагувати</a></li>
            <li class="mdl-color--red-100"><button class="mdl-menu__item delete-btn" value="/news/delete/<?=$item['id']?>" onclick="ajax_func(this)">Видалити</button></li>
        </ul>
    </div>
</div>
<hr>
<?php } ?>
